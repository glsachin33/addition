package com.sachin.addition;

import org.springframework.stereotype.Component;

@Component
public class SumImpl implements Sum {

	public int add(int a,int b)
	{
		return a+b;
	}
	
}
