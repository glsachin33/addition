package com.sachin.addition;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.*;
@SpringBootTest
class AdditionApplicationTests {

	@Autowired
	public SumImpl sum;
	
	@Test
	void testSum() {
		assertEquals(10,sum.add(8,2));
	}

}
