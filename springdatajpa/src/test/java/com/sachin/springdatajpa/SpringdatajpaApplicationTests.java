package com.sachin.springdatajpa;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.sachin.springdatajpa.entities.Student;
import com.sachin.springdatajpa.repos.StudentRepository;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SpringdatajpaApplicationTests {

	@Autowired
	private StudentRepository repository;
	
	@Test
	void testSaveStudent() {
		
		
		Student student = new Student();
		student.setId(1l);
		student.setName("Sachin");
		student.setTestscore(100);
		
		//Create
		repository.save(student);
		System.out.println(repository.findById(1l).get().getName());
		
		//Read and Update
		if(repository.findById(1l).get().getName().equals("Sachin")) {
			
			student.setName("Bharath");
			repository.save(student);
		}
		System.out.println(repository.findById(1l).get().getName());
		
		//Assert
		Student savedStudent=repository.findById(1l).get();
		assertNotNull(savedStudent);
		
		//Delete
		repository.deleteById(1l);
		assertThat(repository.count()).isEqualTo(0);
		
		
	}

}
